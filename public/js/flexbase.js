angular.module('flexbase', [
	'ngRoute',
	'ngHandsontable'
]);

angular.module('flexbase').config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl: 'views/home.ng.html',
		controller: 'HomeController'
	}).when('/odata-browser', {
		templateUrl: 'views/odata-browser.ng.html',
		controller: 'OdataBrowserController'
	}).when('/collections/:collectionId', {
		templateUrl: 'views/single-collection.ng.html',
		controller: 'SingleCollectionController'
	}).otherwise({
		redirectTo: '/home'
	});
}]);


angular.module('flexbase').controller('HomeController', ['$rootScope',
	function($rootScope) {

		$rootScope.selectedCollection = {};

	}]);

angular.module('flexbase').controller('NewCollectionController', ['$scope', 'CollectionsFactory', '$rootScope',
	function($scope, CollectionsFactory, $rootScope) {

		$scope.newCollectionName = '';

		$scope.createCollection = function() {
			CollectionsFactory.createNew($scope.newCollectionName).success(function(collection) {
				$rootScope.$emit('new-collection-added-by-modal', collection);
			});
		};

	}]);
angular.module('flexbase').controller('OdataBrowserController', ['$scope',
	function($scope) {

		$scope.url = '';

		$scope.endpoint = '';

		$scope.showGrid = false;

		$scope.browseData = function() {
			$scope.endpoint = '/proxy?url=' + $scope.url;
			$scope.showGrid = true;
		};

	}]);

angular.module('flexbase').controller('SidebarController', ['$scope', 'CollectionsFactory', '$rootScope',
	function ($scope, CollectionsFactory, $rootScope) {

		$rootScope.selectedCollection = {};

		CollectionsFactory.getAll().success(function (response) {
			$scope.collections = response.value;
		});

		$scope.selectCollection = function(collection) {
			$rootScope.selectedCollection = collection;
		};

		$rootScope.$on('new-collection-added-by-modal', function(event, collection) {
			$scope.collections.push(collection);
		});

	}]);

angular.module('flexbase').controller('SingleCollectionController', ['$scope', '$rootScope', '$location', 'ExtraFunctions', 'CollectionsFactory', '$routeParams',
	function ($scope, $rootScope, $location, ExtraFunctions, CollectionsFactory, $routeParams) {

		$scope.endpoint = CollectionsFactory.getEndpoint($routeParams.collectionId);
		CollectionsFactory.getOne($routeParams.collectionId).success(function(response) {
			$scope.rows = response.value;
		});

	}]);

angular.module('flexbase').directive('dxgrid', [function(){
	return {
		restrict: 'E',
		scope: {
			endpoint: '=endpoint'
		},
		link: function($scope, element, attrs) {
		    element.dxDataGrid({
		        dataSource: {
		            paginate: true,
		            store: new DevExpress.data.ODataStore({
		                type: 'odata' ,
		                version: 4,
		                url: $scope.endpoint,
		                key: 'ID',
		                keyType: 'Int32'
		            })
		        },
		        editing: {
		            editMode: 'cell',
		            editEnabled: true,
		            removeEnabled: true,
		            insertEnabled: true
		        },
		        paging: {pageSize: 10},
		        rowAlternationEnabled: true,
		        columnAutoWidth: true,
		        loadPanel: false
		    });

		    $(document).ajaxComplete(function() {
		    	var $rowsElement = $('.dx-datagrid-rowsview');
		    	if ($rowsElement.length) {
		    		// element exists
		    		$rowsElement.first().css('height', 'auto');
		    	}
		    });

		}
	};
}]);;
angular.module('flexbase').factory('CollectionsFactory', ['$http',
	function($http) {

		var CollectionsFactory = {};

		function generateUrl(url) {
			return '/proxy?url=http://services.odata.org/V4/(S(4fmvspknagthko45fr2vsfwa))/OData/OData.svc/' + url;
		}

		CollectionsFactory.getAll = function() {

			var promise = $http.get(generateUrl('/'));

			return promise;

		};

		CollectionsFactory.getOne = function(collectionId) {

			var promise = $http.get(generateUrl('/' + collectionId));

			return promise;

		};

		CollectionsFactory.createNew = function(name) {
			return $http.post('http://localhost:8000/api/collections', {
				Name: name
			});
		};

		CollectionsFactory.createNewColumn = function(columnObj) {
			var promise = $http.post('http://localhost:8000/api/columns', columnObj);
			return promise;
		};

		CollectionsFactory.createNewRow = function(collectionId, newRowObject) {
			$http({
				method: 'POST',
				url: generateUrl('/' + collectionId),
				headers: { 'Content-Type': 'application/json' },
				data: newRowObject
			});
		};

		CollectionsFactory.getEndpoint = function(collectionId) {
			return generateUrl('/'+collectionId);
		};

		return CollectionsFactory;

	}]);
angular.module('flexbase').factory('ExtraFunctions', [
	function() {

		var ExtraFunctions = {};

		ExtraFunctions.generateColumnsForHandsontable = function(rawColumns) {
			var generatedArray = [];
			for (var i = 0; i < rawColumns.length; i++) {
				var currentColumnName = rawColumns[i].Name;
				generatedArray.push(currentColumnName);
			}
			return generatedArray;
		};

		ExtraFunctions.generateRowsForHandsontable = function(rawRows) {
			var generatedObjectToPush = {};
			for (key in rawRows[0]) {
				generatedObjectToPush[key] = '';
			}
			rawRows.push(generatedObjectToPush);
			return rawRows;
		};

		ExtraFunctions.generateEmptyRow = function(prototypeObj) {
			var objectToGenerate = {};
			for (var key in prototypeObj) {
				objectToGenerate[key] = '';
			}
			return objectToGenerate;
		};

		return ExtraFunctions;

	}]);
