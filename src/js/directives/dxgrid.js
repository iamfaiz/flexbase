angular.module('flexbase').directive('dxgrid', [function(){
	return {
		restrict: 'E',
		scope: {
			endpoint: '=endpoint'
		},
		link: function($scope, element, attrs) {
		    element.dxDataGrid({
		        dataSource: {
		            paginate: true,
		            store: new DevExpress.data.ODataStore({
		                type: 'odata' ,
		                version: 4,
		                url: $scope.endpoint,
		                key: 'ID',
		                keyType: 'Int32'
		            })
		        },
		        editing: {
		            editMode: 'cell',
		            editEnabled: true,
		            removeEnabled: true,
		            insertEnabled: true
		        },
		        paging: {pageSize: 10},
		        rowAlternationEnabled: true,
		        columnAutoWidth: true,
		        loadPanel: false
		    });

		    $(document).ajaxComplete(function() {
		    	var $rowsElement = $('.dx-datagrid-rowsview');
		    	if ($rowsElement.length) {
		    		// element exists
		    		$rowsElement.first().css('height', 'auto');
		    	}
		    });

		}
	};
}]);;