angular.module('flexbase').factory('ExtraFunctions', [
	function() {

		var ExtraFunctions = {};

		ExtraFunctions.generateColumnsForHandsontable = function(rawColumns) {
			var generatedArray = [];
			for (var i = 0; i < rawColumns.length; i++) {
				var currentColumnName = rawColumns[i].Name;
				generatedArray.push(currentColumnName);
			}
			return generatedArray;
		};

		ExtraFunctions.generateRowsForHandsontable = function(rawRows) {
			var generatedObjectToPush = {};
			for (key in rawRows[0]) {
				generatedObjectToPush[key] = '';
			}
			rawRows.push(generatedObjectToPush);
			return rawRows;
		};

		ExtraFunctions.generateEmptyRow = function(prototypeObj) {
			var objectToGenerate = {};
			for (var key in prototypeObj) {
				objectToGenerate[key] = '';
			}
			return objectToGenerate;
		};

		return ExtraFunctions;

	}]);
