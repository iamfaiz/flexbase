angular.module('flexbase').factory('CollectionsFactory', ['$http',
	function($http) {

		var CollectionsFactory = {};

		function generateUrl(url) {
			return '/proxy?url=http://services.odata.org/V4/(S(4fmvspknagthko45fr2vsfwa))/OData/OData.svc/' + url;
		}

		CollectionsFactory.getAll = function() {

			var promise = $http.get(generateUrl('/'));

			return promise;

		};

		CollectionsFactory.getOne = function(collectionId) {

			var promise = $http.get(generateUrl('/' + collectionId));

			return promise;

		};

		CollectionsFactory.createNew = function(name) {
			return $http.post('http://localhost:8000/api/collections', {
				Name: name
			});
		};

		CollectionsFactory.createNewColumn = function(columnObj) {
			var promise = $http.post('http://localhost:8000/api/columns', columnObj);
			return promise;
		};

		CollectionsFactory.createNewRow = function(collectionId, newRowObject) {
			$http({
				method: 'POST',
				url: generateUrl('/' + collectionId),
				headers: { 'Content-Type': 'application/json' },
				data: newRowObject
			});
		};

		CollectionsFactory.getEndpoint = function(collectionId) {
			return generateUrl('/'+collectionId);
		};

		return CollectionsFactory;

	}]);