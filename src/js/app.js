angular.module('flexbase', [
	'ngRoute',
	'ngHandsontable'
]);

angular.module('flexbase').config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl: 'views/home.ng.html',
		controller: 'HomeController'
	}).when('/odata-browser', {
		templateUrl: 'views/odata-browser.ng.html',
		controller: 'OdataBrowserController'
	}).when('/collections/:collectionId', {
		templateUrl: 'views/single-collection.ng.html',
		controller: 'SingleCollectionController'
	}).otherwise({
		redirectTo: '/home'
	});
}]);
