angular.module('flexbase').controller('NewCollectionController', ['$scope', 'CollectionsFactory', '$rootScope',
	function($scope, CollectionsFactory, $rootScope) {

		$scope.newCollectionName = '';

		$scope.createCollection = function() {
			CollectionsFactory.createNew($scope.newCollectionName).success(function(collection) {
				$rootScope.$emit('new-collection-added-by-modal', collection);
			});
		};

	}]);