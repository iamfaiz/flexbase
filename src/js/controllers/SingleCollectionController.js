angular.module('flexbase').controller('SingleCollectionController', ['$scope', '$rootScope', '$location', 'ExtraFunctions', 'CollectionsFactory', '$routeParams',
	function ($scope, $rootScope, $location, ExtraFunctions, CollectionsFactory, $routeParams) {

		$scope.endpoint = CollectionsFactory.getEndpoint($routeParams.collectionId);
		CollectionsFactory.getOne($routeParams.collectionId).success(function(response) {
			$scope.rows = response.value;
		});

	}]);
