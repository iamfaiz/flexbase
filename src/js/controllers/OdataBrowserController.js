angular.module('flexbase').controller('OdataBrowserController', ['$scope',
	function($scope) {

		$scope.url = '';

		$scope.endpoint = '';

		$scope.showGrid = false;

		$scope.browseData = function() {
			$scope.endpoint = '/proxy?url=' + $scope.url;
			$scope.showGrid = true;
		};

	}]);
