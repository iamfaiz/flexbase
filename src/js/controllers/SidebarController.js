angular.module('flexbase').controller('SidebarController', ['$scope', 'CollectionsFactory', '$rootScope',
	function ($scope, CollectionsFactory, $rootScope) {

		$rootScope.selectedCollection = {};

		CollectionsFactory.getAll().success(function (response) {
			$scope.collections = response.value;
		});

		$scope.selectCollection = function(collection) {
			$rootScope.selectedCollection = collection;
		};

		$rootScope.$on('new-collection-added-by-modal', function(event, collection) {
			$scope.collections.push(collection);
		});

	}]);
