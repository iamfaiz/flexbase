var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('concat', function() {
	gulp.src('./src/js/**/*.js')
		.pipe(concat('flexbase.js'))
		.pipe(gulp.dest('./public/js'));
});

gulp.task('watch', function() {
	gulp.watch(['./src/js/**/*.js'], ['concat']);
});
