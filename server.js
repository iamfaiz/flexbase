var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

app.use(cors());
app.use(bodyParser({ strict: false }));
app.use(express.static('./public'));

app.all('/proxy', function(req, res) {
	var method = req.method;
	var data = req.body || {};
	data['@odata.type'] = 'ODataDemo.Product';
	if ( method === 'MERGE' ) {
		method = 'PATCH';
	}
	var options = {
		method: method,
		url: req.query.url,
		body: data,
		json: true,
		headers: {
			'Content-Type': 'application/json'
		}
	};
	request(options, function(error, response, body) {
		if (error) console.log(error);
		res.statusCode = response.statusCode;
		res.send(response.body);
	});
});

app.listen(3000, function() {
	console.log('Listening on port 3000');
});